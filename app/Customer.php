<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded = [];
    protected $primaryKey =  'cid';

    protected $table = 'customer';
}
