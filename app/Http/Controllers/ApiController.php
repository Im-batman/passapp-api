<?php

namespace App\Http\Controllers;

use App\Category;
use App\Customer;
use App\Event;
use App\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ApiController extends Controller
{


	public function test( Request $request ) {
		echo "api is alive!!";
	}
	

	public function registerUser(Request $request  ) {

		try{

			$customer = new Customer();
			$customer->email = $request->input('email');
			$customer->password = bcrypt($request->input('password'));
			$customer->username = $request->input('username');
			$customer->save();

			return response(['status'=>'success','customer'=> $customer],200);

		}catch (\Exception $exception){
			return $exception->getMessage();
		}

	}


	public function loginUser(Request $request) {

		$email = $request->input('email');
		$password = $request->input('password');
		$customer = Customer::where('email',$email)->first();

		try{
			if (count($customer) > 0){
				if (password_verify($password,$customer->password)){
					$token = str_random(60);
					$customer->api_token = $token;
					$customer->save();
					return response(['status'=>'success','customer'=>$customer],200);
				}
			}
		}catch (\Exception $exception){
			return $exception->getMessage();
		}
	}


	public function updateUser(Request $request) {

		$user = Customer::update(\request()->all());
		return $user;
	}


	public function createCategories( Request $request ) {

		$cat  =  new Category();
		$cat->name = $request->input('name');
		$cat->description = $request->input('description');
		$cat->save();
		return response(['status'=> 'success','data'=> $cat],200);
	}


	public function createOrganizations(Request $request) {

		$org = new Organization();
		$org->cid = $request->input('cid');
		$org->name = $request->input('name');
		$org->address = $request->input('address');
		$org->phone_number = $request->input('phone');
		$org->save();
		return response(['status'=>'success', 'data'=>$org],200);

	}


	public function createEvent(Request $request) {
	
	try{


		if($request->hasFile('image')){
			$fileName = $request->file('image')->getClientOriginalName();
			$request->file('image')->move('uploads',$fileName);
			$imageUrl = url('uploads/' . $fileName);
		}


		$event = new Event();
		$event->orid = $request->input('orid');
		$event->catid = $request->input('catid');
		$event->name = $request->input('name');
		$event->description = $request->input('description');
		$event->fee = $request->input('fee');
		$event->type = $request->input('type');
		$event->city = $request->input('city');
		$event->event_times = $request->input('event_times');
		$event->start_time = $request->input('start_time');
		$event->end_time = $request->input('end_time');
		$event->imageUrl = $imageUrl;

		$event->save();





		return response(['status'=> 'success','data'=>$event],200);

	}catch (\Exception $exception){
		return response(['error'=> $exception->getMessage()],400);
	}
	
	}

	public function getAllEvents(Request $request) {


		if (Input::has('city')){

			$city = Input::get('city');
			$events = Event::where('city',$city)->get();
			return response(['status'=>'success','data'=>$events],200);

		}else{
			$events = Event::orderBy('created_at','desc')->paginate(40);
			return response(['status'=>'success','events'=>$events],200);
		}


	}


	public function searchEvent(Request $request) {

		if (Input::has('search-term')){

			$term = Input::get('search-term');
			$event = Event::where('name','like',"%$term%")->pagiante(10);

			return response(['status'=>'successful','data'=>$event]);

		}else{
			return response(['status'=> 'success','data'=> []],200);
		}


	}



}
