<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $guarded = [];
    protected $table = 'organizations';
    protected $primaryKey = 'orid';
}
