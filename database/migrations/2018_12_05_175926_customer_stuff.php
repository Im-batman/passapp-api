<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomerStuff extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //



	    Schema::create('customer', function (Blueprint $table) {
		    $table->increments('cid');
		    $table->string('email')->index();
		    $table->string('password');
		    $table->string('username')->nullable();
		    $table->string('imageUrl')->nullable();
		    $table->string('api_token',60)->uniqid()->nullable();
		    $table->timestamps();
	    });


	    Schema::create('organization_catergory', function (Blueprint $table) {
		    $table->increments('catid');
		    $table->string('name')->unique();
		    $table->string('description',80);
		    $table->timestamps();
	    });


	    Schema::create('organizations',function (Blueprint $table){
		    $table->increments('orid');
		    $table->integer('cid');
		    $table->string('name');
		    $table->string('address');
		    $table->string('phone_number');
		    $table->integer('catid')->nullable();
		    $table->timestamps();
	    });


	    Schema::create('events',function (Blueprint $table){
		    $table->increments('eid');
		    $table->integer('orid');
		    $table->integer('catid');
		    $table->enum('type',['onetime','repeated']);
		    $table->string('name');
		    $table->string('imageUrl');
		    $table->string('description',5000);
		    $table->string('city');
		    $table->integer('fee')->default(0);
		    $table->enum('event_times',['daily','nights','weekends','specific_date']);
		    $table->dateTime('start_time')->nullable();
		    $table->dateTime('end_time')->nullable();
		    $table->timestamps();
	    });





    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
