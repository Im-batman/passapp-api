<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('user/register','ApiController@registerUser');
Route::post('user/login','ApiController@loginUser');


Route::get('user/get-all-events','ApiController@getAllEvents');
Route::get('test','ApiController@test');



Route::group(['middleware' => ['auth:api']],function (){


});

Route::post('create-category','ApiController@createCategories');
Route::post('create-event','ApiController@createEvent');
Route::post('create-organization','ApiController@createOrganizations');
